<?php

	class ContactsController extends BaseController {

		/**
		 * Contact Repository
		 *
		 * @var Contact
		 */
		protected $contact;

		public function __construct(Contact $contact)
		{
			$this->contact = $contact;
		}

		/**
		 * Display a listing of the resource.
		 *
		 * @return Response
		 */
		public function index()
		{
			$contacts = $this->contact->all();

			return View::make('contacts.index', compact('contacts'));
		}

		/**
		 * Show the form for creating a new resource.
		 *
		 * @return Response
		 */
		public function create()
		{
			return View::make('contacts.create');
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @return Response
		 */
		public function store()
		{
			$input = Input::all();
			$validation = Validator::make($input, Contact::$rules);

			if ($validation->passes())
			{
				$this->contact->create($input);

				return Redirect::route('contacts.index');
			}

			return Redirect::route('contacts.create')
				->withInput()
				->withErrors($validation)
				->with('message', 'There were validation errors.');
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function show($id)
		{
			$contact = $this->contact->findOrFail($id);

			if($contact->personal) {
				$personal = PersonalContact::where('contact_id', '=', $contact->id)->firstOrFail();
				$compact_var = 'personal';
			} else {
				$business = BusinessContact::where('contact_id', '=', $contact->id)->firstOrFail();
				$compact_var = 'business';
			}

			return View::make('contacts.show', compact('contact', $compact_var));
		}

		/**
		 * Display random resource.
		 *
		 * @return Response
		 */
		public function random()
		{
			$contact = Contact::all()->random(1);
			$output["name"] = $contact->name;
			$output["url"] = action('ContactsController@show', $contact->id);
			if($contact->personal) {
				$personal = PersonalContact::where('contact_id', '=', $contact->id)->firstOrFail();
				$output["photo"] = $personal->photo;
			}
			return $output;
		}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function edit($id)
		{
			//
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function update($id)
		{
			//
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function destroy($id)
		{
			//
		}

	}