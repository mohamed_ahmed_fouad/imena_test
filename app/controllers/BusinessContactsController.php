<?php

	class BusinessContactsController extends BaseController {

		/**
		 * Contact Repository
		 *
		 * @var Contact
		 */
		protected $contact;
		protected $business;

		public function __construct(Contact $contact, BusinessContact $business)
		{
			$this->contact = $contact;
			$this->business = $business;
		}

		/**
		 * Show the form for creating a new resource.
		 *
		 * @return Response
		 */
		public function create()
		{
			return View::make('business.create');
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @return Response
		 */
		public function store()
		{
			$input = Input::all();
			$validation = Validator::make($input, array_merge(Contact::$rules, BusinessContact::$rules));

			if ($validation->passes())
			{

				$contact = new Contact;
				$contact->name = $input["name"];
				$contact->email = $input["email"];
				$contact->number = $input["number"];
				$contact->personal = false;
				$contact->save();

				$business_input = ["fax" => $input["fax"], "website" => $input["website"], "contact_id" => $contact->id];

				$this->business->create($business_input);
				return Redirect::route('contacts.index');
			}

			return Redirect::route('business_contacts.create')
				->withInput()
				->withErrors($validation)
				->with('message', 'There were validation errors.');
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function show($id)
		{
			//
		}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function edit($id)
		{
			//
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function update($id)
		{
			//
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function destroy($id)
		{
			//
		}

	}