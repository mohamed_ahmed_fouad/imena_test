<?php

	class PersonalContactsController extends BaseController {

		/**
		 * Contact Repository
		 *
		 * @var Contact
		 */
		protected $contact;
		protected $personal;

		public function __construct(Contact $contact, PersonalContact $personal)
		{
			$this->contact = $contact;
			$this->personal = $personal;
		}

		/**
		 * Show the form for creating a new resource.
		 *
		 * @return Response
		 */
		public function create()
		{
			return View::make('personal.create');
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @return Response
		 */
		public function store()
		{
			$input = Input::all();
			$validation = Validator::make($input, array_merge(Contact::$rules, PersonalContact::$rules));

			if ($validation->passes() && Input::hasFile('photo'))
			{

				$contact = new Contact;
				$contact->name = $input["name"];
				$contact->email = $input["email"];
				$contact->number = $input["number"];
				$contact->personal = true;
				$contact->save();

				$extension = Input::file('photo')->getClientOriginalExtension();
				$file_name = $contact->id.".".$extension;
				Input::file('photo')->move("photos", $file_name);

				$personal_input = ["home_address" => $input["home_address"], "birth_day" => $input["birth_day"], "photo" => $file_name, "contact_id" => $contact->id];

				$this->personal->create($personal_input);
				return Redirect::route('contacts.index');
			}

			return Redirect::route('personal_contacts.create')
				->withInput()
				->withErrors($validation)
				->with('message', 'There were validation errors.');
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function show($id)
		{
			//
		}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function edit($id)
		{
			//
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function update($id)
		{
			//
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int  $id
		 * @return Response
		 */
		public function destroy($id)
		{
			//
		}

	}