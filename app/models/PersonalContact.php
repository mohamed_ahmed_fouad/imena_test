<?php

	class PersonalContact extends Eloquent {
		protected $guarded = array();

		public static $rules = array(
			'home_address' => 'required',
			'birth_day' => 'required',
			'photo' => 'required|image',
			'captcha' => 'required|captcha',
		);

		public function contact ()
		{
			return $this->belongsTo('Contact');
		}
	}