<?php

	class Contact extends Eloquent {
		protected $guarded = array();

		public static $rules = array(
			'name' => 'required',
			'email' => 'required|email',
			'number' => 'required|numeric'
		);

	}