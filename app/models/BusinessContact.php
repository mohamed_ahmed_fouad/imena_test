<?php

	class BusinessContact extends Eloquent {
		protected $guarded = array();

		public static $rules = array(
			'fax' => 'required',
			'website' => 'required',
			'captcha' => 'required|captcha',
		);

		public function contact ()
		{
			return $this->belongsTo('Contact');
		}
	}