<?php

	Route::get('/', array('uses' => 'ContactsController@index'));
	Route::get('random_contact', array('uses' => 'ContactsController@random'));


	Route::resource('contacts', 'ContactsController');
	Route::resource('personal_contacts', 'PersonalContactsController');
	Route::resource('business_contacts', 'BusinessContactsController');