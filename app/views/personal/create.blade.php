@extends('layouts.scaffold')

@section('main')

<h1>Create Contact</h1>

{{ Form::open(array('route' => 'personal_contacts.store', 'files' => true)) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>

        <li>
            {{ Form::label('email', 'Email:') }}
            {{ Form::text('email') }}
        </li>

        <li>
            {{ Form::label('number', 'Number:') }}
            {{ Form::text('number') }}
        </li>

        <li>
            {{ Form::label('home_address', 'Home Address:') }}
            {{ Form::text('home_address') }}
        </li>

        <li>
            {{ Form::label('birth_day', 'Birth day:') }}
            {{ Form::text('birth_day') }}
        </li>

        <li>
            {{ Form::label('photo', 'Photo:') }}
            {{ Form::file('photo') }}
        </li>

        {{ HTML::image(Captcha::img(), 'Captcha image') }}
                {{  Form::text('captcha') }}

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop