<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		{{ HTML::style('css/bootstrap.css'); }}
		<style>
			table form { margin-bottom: 0; }
			form ul { margin-left: 0; list-style: none; }
			.error { color: red; font-style: italic; }
			body { padding-top: 20px; }
			#sidebar {
			    width: 220px;
			    min-height: 200px;
			    padding-right: 20px;
			    float: left;
			}
			.table {
                width: initial;
            }
		</style>
	</head>

    {{ HTML::script('js/jquery.js'); }}
    {{ HTML::script('js/bootstrap.js'); }}

	<body>



		<div class="container">

			@if (Session::has('message'))
				<div class="flash alert">
					<p>{{ Session::get('message') }}</p>
				</div>
			@endif

             @yield('sidebar')

            <div id="main">
                @yield('main')
            </div>

		</div>

    <script>
        $.getJSON("/random_contact", function(result){
            $('#random_name').text(result.name);
            $('#random_url').attr("href", result.url);
            if(result.photo) {
                $('#random_photo').attr("src", "/photos/" + result.photo);
            }
        });
    </script>
	</body>

</html>