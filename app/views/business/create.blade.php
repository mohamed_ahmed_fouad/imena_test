@extends('layouts.scaffold')

@section('main')

<h1>Create Contact</h1>

{{ Form::open(array('route' => 'business_contacts.store')) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>

        <li>
            {{ Form::label('email', 'Email:') }}
            {{ Form::text('email') }}
        </li>

        <li>
            {{ Form::label('number', 'Number:') }}
            {{ Form::text('number') }}
        </li>

        <li>
            {{ Form::label('fax', 'Fax:') }}
            {{ Form::text('fax') }}
        </li>

        <li>
            {{ Form::label('website', 'Website:') }}
            {{ Form::text('website') }}
        </li>

        {{ HTML::image(Captcha::img(), 'Captcha image') }}
        {{  Form::text('captcha') }}

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop