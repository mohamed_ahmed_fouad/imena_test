@extends('layouts.scaffold')

@section('main')

<h1>Show Contact</h1>

<p>{{ link_to_route('contacts.index', 'Return to all contacts') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Name</th>
            <th>Email</th>
            <th>Number</th>

            @if ($contact->personal)
                <th>Home Address</th>
                <th>Birth day</th>
            @else
                <th>Fax</th>
                <th>Website</th>
            @endif

            <th>Action</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $contact->name }}}</td>
            <td>{{{ $contact->email }}}</td>
            <td>{{{ $contact->number }}}</td>

			@if ($contact->personal)
			    <td>{{{ $personal->home_address }}}</td>
			    <td>{{{ $personal->birth_day }}}</td>
			    <td><!-- Button HTML (to Trigger Modal) -->
                        <a href="#myModal" class="btn btn-primary" data-toggle="modal">Show Photo & Address</a>

                        <!-- Modal HTML -->
                        <div id="myModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Photo & Home Address</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>{{{ $personal->home_address }}}</p>
                                        <img src="{{asset('photos/'.$personal->photo)}}" style="max-width: 570px;max-height: 400px;">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div></td>
			@else
            	<td>{{{ $business->fax }}}</td>
                <td>{{{ $business->website }}}</td>
                <td><a class="btn btn-primary" href="mailto:{{{ $contact->email }}}?Subject=Hello%20iMENA" target="_top">Send Mail</a></td>
            @endif
		</tr>
	</tbody>
</table>

@stop