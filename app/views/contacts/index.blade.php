@extends('layouts.scaffold')

@section('sidebar')
<div id="sidebar">
    <h3>Random Contact</h3>
    <a href="#" id="random_url">
        <img src="{{asset('photos/default.jpg')}}" id="random_photo" width="50" height="50">
        <span id="random_name"></span>
    </a>
</div>
@stop

@section('main')

<h1>All Contacts</h1>

<p>{{ link_to_route('personal_contacts.create', 'Add new Personal contact') }} | {{ link_to_route('business_contacts.create', 'Add new Business contact') }} </p>

@if ($contacts->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Number</th>
				<th>Type</th>
				<th>Action</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($contacts as $contact)
				<tr>
					<td>{{{ $contact->name }}}</td>
					<td>{{{ $contact->email }}}</td>
					<td>{{{ $contact->number }}}</td>
					@if ($contact->personal)
                        <td>Personal</td>
                    @else
                        <td>Business</td>
                    @endif
					<td>{{ link_to_route('contacts.show', 'Show', array($contact->id), array('class' => 'btn btn-info')) }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no contacts
@endif

@stop